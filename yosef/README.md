# Yosef

--------------------------------

> Shikhar Patel	

--------------------------------

```
Hostname: yosef
IP: 10.0.2.5
```

## Penetrating in the Box

### Nmap Enumeration

- 22, 80 ports are open.

- There is a folder named `adminstration` on the website. I got to know this when I saw the write-up videos on Youtube.

- So after bruteforcing into that diretory I found out a bunch of directories that could help me get access into the machine.

- But I was not able to visit that directory as I was forbidden to access to that folder.

- So I tried to google and find out how can I access those folders.

- And I got a way where I can bypass the authentication, by adding a header X-Forwarding-For.

- So I fired up BurpSuite and tried to forge the headers using that.

- After adding `X-Forwarding For: localhost` in the header intercepted in burpsuite I got access to the webpage, and I saw an upload file page which prompted me to upload files. 

- So, as usual I tried to upload a php-reverse-shell but could'nt do it, as there was some sanitixation going on in the back so I tried and uploaded .php file and then changed `Content-Type: application/x-php` to `Content-Type: image/png` and then i forwarded the request which went through.

- After uploading the script I turned on my nc listener on my terminal to recieve the reverse shell and navigated to the filename which it uploaded the file and that is how I got my reverse shell.

To Stabilize the shell
```bash
python -c "import pty; pty.spawn('/bin/bash')"
export TERM=screen
	stty raw -echo (This should me in our terminal)
	Then we go back in the reverse shell and type the command 'export TERM=xterm'
```

- Then I navigate to the home directory and then I get the user flag.

```
Username: yosef
User-Flag: c3NoIDogCnVzZXIgOiB5b3VzZWYgCnBhc3MgOiB5b3VzZWYxMjM=
User-Flag-Decoded: 'ssh : user : yousef' 

```

## Privilege Escalation

- After enumerating the machine with linpeas.sh, I found that this machine is 

`Linux yousef-VirtualBox '3.13.0-24-generic' #46-Ubuntu SMP Thu Apr 10 19:08:14 UTC 2014 i686 i686 i686 GNU/Linux`

- And the version of this Ubuntu is vulnerable to `CVE-2015-1328` exploits the "overlayfs incorrect permission handling" and "FS_USERNS_MOUNT[Filesystem mounts in user namespaces]"(which basically allows file systems to be mounted on the user namespaces.)

```
User namespaces are a sandbox within which an otherwise unprivileged user can appear to be root. Given a kernel with user namespaces enabled, any user can create such a namespace and, while they are running within it, do anything that root is allowed to do. The intent is that system administrators can allow the creation of these namespaces by unprivileged users, secure in the knowledge that any actions carried out within them will, in the context of the wider system, be constrained by the user's global credentials. There are some limits to what can be done within user namespaces though, with the mounting of filesystems being near the top of the list. A recent attempt to ease that limitation shows just how hard the problem is to solve.
```

- So I just copy paste the exploit in the file called exploit.c which I created in /tmp folder.

- And then after compiling and running it I get my root shell and thereby the root flag.

```
Username: root
Root-Flag: WW91J3ZlIGdvdCB0aGUgcm9vdCBDb25ncmF0dWxhdGlvbnMgYW55IGZlZWRiYWNrIGNvbnRlbnQgbWUgdHdpdHRlciBAeTB1c2VmXzEx
Root-Flag-Decoded: You've got the root Congratulations any feedback content me twitter @y0usef_11 
```